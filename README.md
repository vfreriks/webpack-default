# Webpack default front-end setup
- SCSS compilation
- JS transpiling using Babel
- CSS Minification
- SVG sprite generation
- Cache busting + manifest.json

## Installation
Run `npm install` or `yarn` using (at least) Node version 10

## Usage
- `npm run dev` or `yarn dev`: create development build
- `npm run build` or `yarn build`: create production build
- `npm run watch` or `yarn watch`: create dev build and watch files

## Structure
All development files can be found in `/src` in their respective folders. Compiled files will be placed in the dist folder.
Modify `webpack.config.js` to disable cache busting. 
