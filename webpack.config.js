const path = require('path');

// include the js minification plugin
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');
const SVGSpritemapPlugin = require('svg-spritemap-webpack-plugin');

module.exports = {
	entry: ['./src/js/index.js', './src/scss/styles.scss'],
	output: {
		filename: './dist/js/app.min.[hash].js',
		path: path.resolve(__dirname)
	},
	module: {
		rules: [
			// perform js babelization on all .js files
			{
				test: /\.js$/,
				exclude: /node_modules/,
				use: {
					loader: "babel-loader",
					options: {
						presets: ['@babel/preset-env'],
					}
				}
			},
			// compile all .scss files to plain old css
			{
				test: /\.(sass|scss)$/,
				use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader']
			},
		]
	},
	plugins: [
		// extract css into dedicated file
		new MiniCssExtractPlugin({
			filename: './dist/css/styles.min.[hash].css'
		}),
		new CleanWebpackPlugin({
			cleanOnceBeforeBuildPatterns: ['./dist/js/*', './dist/css/*']
		}),
		new SVGSpritemapPlugin('src/svg/*.svg', {
			output: {
				filename: './dist/svg/spritemap.svg'
			},
			sprite: {
				prefix: 'sprite-'
			}
		})
	],
	optimization: {
		minimizer: [
			// enable the js minification plugin
			new UglifyJSPlugin({
				cache: true,
				parallel: true
			}),
			// enable the css minification plugin
			new OptimizeCSSAssetsPlugin({}),
		]
	}
};
